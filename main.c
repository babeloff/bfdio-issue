#include <stdio.h>
#include <errno.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>

#define DEBUG 1
#define debug_print(...) \
            do { if (DEBUG) fprintf(stderr, __VA_ARGS__); } while (0)

/**
 * copied from https://chromium.googlesource.com/native_client/nacl-binutils/+/gdb-7.9.1-release/include/filenames.h
 */
#define IS_DIR_SEPARATOR_1(dos_based, c)				\
  (((c) == '/')								\
   || (((c) == '\\') && (dos_based)))
#define IS_UNIX_DIR_SEPARATOR(c) IS_DIR_SEPARATOR_1 (0, c)

/* Mark FILE as close-on-exec.  Return FILE.  FILE may be NULL, in
   which case nothing is done.  */
static FILE *
close_on_exec(FILE *file) {
#if defined (HAVE_FILENO) && defined (F_GETFD)
  if (file)
    {
      int fd = fileno (file);
      int old = fcntl (fd, F_GETFD, 0);
      if (old >= 0)
      fcntl (fd, F_SETFD, old | FD_CLOEXEC);
    }
#endif
  return file;
}

FILE *
_bfd_real_fopen (const char *filename, const char *modes)
{
#ifdef VMS

  // not included

#elif defined (_WIN32)
  /* PR 25713: Handle extra long path names possibly containing '..' and '.'. */

  wchar_t **     lpFilePart = {NULL};
  const wchar_t  prefix[] = L"\\\\?\\";
  const size_t   partPathLen = strlen(filename) + 1;

  /* Converting the partial path from ascii to unicode.
     1) get the length: Calling with lpWideCharStr set to null returns the length.
     2) convert the string: Calling with cbMultiByte set to -1 includes the terminating null.  */
  size_t         partPathWSize = MultiByteToWideChar (CP_UTF8, 0, filename, -1, NULL, 0);
  wchar_t *      partPath = calloc (partPathWSize, sizeof(wchar_t));

  MultiByteToWideChar (CP_UTF8, 0, filename, -1, partPath, partPathWSize);

  /* Convert any UNIX style path separators into the DOS form i.e. backslash separator.  */
  for (size_t ix = 0; ix < partPathLen; ix++)
    if (IS_UNIX_DIR_SEPARATOR(filename[ix])) {
      partPath[ix] = '\\';
    }

  /* Getting the full path from the provided partial path.
     1) get the length:
     2) resolve the path.  */
  long           fullPathWSize = GetFullPathNameW (partPath, 0, NULL, lpFilePart);
  wchar_t *      fullPath = calloc (fullPathWSize + sizeof(prefix) + 1, sizeof(wchar_t));

  wcscpy (fullPath, prefix);
  int            prefixLen = sizeof(prefix) / sizeof(wchar_t);
  wchar_t*       fullPathOffset = fullPath + prefixLen - 1;
  GetFullPathNameW (partPath, fullPathWSize, fullPathOffset, lpFilePart);
  free (partPath);

  /* It is non-standard for modes to exceed 16 characters.  */
  wchar_t        modesW[16];
  MultiByteToWideChar (CP_UTF8, 0, modes, -1, modesW, sizeof(modesW));

  debug_print("open: mode  = %ls, fullPath = %ls\n", modesW, fullPath);
  FILE *         file = _wfopen (fullPath, modesW);
  free (fullPath);
  debug_print("open: errno = %d error = %s\n", errno, strerror(errno));

  return close_on_exec (file);

#elif defined (HAVE_FOPEN64)
  // not included

#else
  return close_on_exec (fopen (filename, modes));
#endif
}

/**
 * https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/fopen-wfopen?view=msvc-170#unicode-support
 * https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getfullpathnamew
 * https://docs.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-multibytetowidechar
 * @return
 */
int main() {
  const char *paths[] = {
      "C:/tools/foo.o",
      "C:/tools/foo.o",
      "C:/tools/./foo.o",
      "C:/tools/msys64/../foo.o",
      "./foo.o",
      "../increasepath/increasepath/increasepath\\increasepath/"
      "increasepath/increasepath/increasepath\\increasepath/increasepath/"
      "increasepath/increasepath/increasepath/increasepath/increasepath/"
      "increasepath/increasepath/increasepath/increasepath/increasepath/"
      "increasepath/increasepath/test.o",
      "../increasepath\\increasepath/.\\./increasepath\\increasepath/"
      "increasepath\\increasepath/../../increasepath\\increasepath/increasepath/increasepath/increasepath\\"
      "increasepath/increasepath/increasepath/increasepath/increasepath/"
      "increasepath/increasepath/increasepath/increasepath/increasepath/"
      "increasepath\\increasepath\\test.o"
  };
  setbuf(stderr, NULL);
  int loopLimit = sizeof(paths) / sizeof(*paths);
  for (int ix = 0; ix < loopLimit; ix++) {
    const char modes[] = "w+b";
    debug_print("====================================================\n");
    debug_print("orig: path = %s\n", paths[ix]);

    FILE *file = _bfd_real_fopen(paths[ix], modes);
  }

  return 0;
}
